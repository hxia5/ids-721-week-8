# IDS 721 Week 8
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-8/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-8/-/commits/main)

The pipeline is already passed, something wrong with gitlab, refer here: https://gitlab.com/gitlab-org/gitlab/-/issues/326513

![alt text](<截屏2024-03-31 下午11.33.57.png>)


## Overview
* This is my repository ofIDS 721 Mini-Project 8 - Rust Command-Line Tool with Testing.

## Purpose
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Data ingestion/processing(data in a `.txt` file)

![alt text](data.png)

## Unit tests

![alt text](test.png)

## Key Steps

1. Install Rust and Cargo.

2. Create a new Rust project using the following command:
```bash
cargo new ids-721-week-8
```

3. Add `encrypt`, `decrypt` and `int_to_ascii` functions to `main.rs` and modify `function_handler` to call them. 

3. Based on my code, add the following dependencies to the Cargo.toml file:
```bash
[dependencies]
clap = { version = "4.3.17", features = ["derive"] }
```

5. Run `cargo build` in terminal, and `cargo run` to use the function.

6. You can do three things with my tool: encrypt a string, decrypt a string, and transfer a int value to a correspond character in ASCII table.

* To encrypt:
```
cargo run --  --message "your string" --encrypt --shift "your shift value"
```


* To decrypt:
```
cargo run --  --message "your string" --decrypt --shift "your shift value"
```

* To transfer a int value to a correspond character in ASCII table, replace "int" with your number:
```
cargo run -- --int-to-ascii --message int
``` 

* To use file format input, change the content after message to the `.txt` file name:
```
cargo run -- --encrypt --message test.txt --shift 3
```

```
cargo run -- --int-to-ascii --message test.txt
```

7. To test the code, run `cargo test --quiet` or `make test`.

## References

1.https://nogibjj.github.io/rust-tutorial/

2.https://gitlab.com/gitlab-org/gitlab/-/issues/326513
